import React from "react";
import "./Item.css";
const Item = props => {
    return (
        <div className="item--main">
            <p className="item--date">{`Created on: ${props.dateTime}`}</p>
            <p className="item--text">{props.title}</p>
            <button onClick={props.click} className="item--button">Read more >></button>


        </div>
    )
};
export default Item;