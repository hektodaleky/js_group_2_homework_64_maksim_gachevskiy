import React, {Component} from "react";
import "./AddPost.css";
import axios from 'axios';
class AddPost extends Component {
    state = {
        post: {
            title: "",
            text: "",
        }
    };
    changeTitle = (event, name) => {
        let post = {...this.state.post};
        post[name] = event.target.value;
        this.setState({post});
    };

    sendData = event => {
        event.preventDefault();
        console.log(this.state);
        axios.post('post.json', {...this.state.post,date:new Date()}).then(response => {
            this.props.history.push({
                pathname: '/'
            });


        });

    };


    render() {
        return (<div className="item--main">
            <h2>Add new post</h2>
            <label htmlFor="add--title">Title</label>
            <input onChange={(event => {
                this.changeTitle(event, "title")
            })} id="add--title" value={this.state.post.title}/>
            <label htmlFor="add--text">Description</label>
            <textarea onChange={(event => {
                this.changeTitle(event, "text")
            })} id="add--text" value={this.state.post.text}></textarea>
            <button onClick={this.sendData} className="add--save">Save</button>

        </div>);
    }
}

export default AddPost;