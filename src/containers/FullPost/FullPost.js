import React, {Component} from "react";
import axios from "axios";
import "./FullPost.css";
class FullPost extends Component {
    state = {
        post: {},
        id: ""


    };

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search);

        for (let param of query.entries()) {


            axios.get(`https://react-lesson-mgachevskiy.firebaseio.com/post/${param[0]}.json`).then(response => {
                this.setState({post: response.data, id: param[0]})
            })
        }


    }

    editText = (id) => {
        this.props.history.push({
            pathname: this.props.history.location.pathname + 'edit/',
            search: this.props.history.location.search

        });
    };
    removeTask = () => {
        axios.delete(`/post/${this.state.id}.json`).then(() => {
            console.log("Deleted!");
            this.props.history.push({
                pathname: '/'
            });        });
    };

    render() {
        return (<div className="full--post">
            <h6 className="full--title">{this.state.post.title}</h6>
            <p className="item--date">{this.state.post.date}</p>
            <p className="full--text">{this.state.post.text}</p>
            <i onClick={() => {
                this.editText(this.state.id)
            }} className="full-change">EDIT</i>
            <i onClick={this.removeTask} className="full--remove">X</i>
        </div> )
    }
}
;
export default FullPost;
