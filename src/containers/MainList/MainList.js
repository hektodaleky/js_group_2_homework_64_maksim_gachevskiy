import React, {Component} from "react";
import Item from "../../components/Item/Item";
import "./MainList.css";
import axios from "axios";
class MainList extends Component {
    state = {
        post: []
    };
    loadItems = (url) => {
        axios.get(url).then(response => {
            const post = [];
            console.log(response.data);

            for (let key in response.data) {
                post.push({...response.data[key], key})
            }
            this.setState({post});
        })
    };

    componentDidMount() {

        this.loadItems('/post.json');



    }

    getFullPost = (id) => {
        this.props.history.push({
            pathname: '/post/',
            search:  id
        });
    };


    render() {
        return (
            <div className="main--window">{
                this.state.post.map(element => {
                    return <Item dateTime={element.date} title={element.title}
                                 click={() => this.getFullPost(element.key)} key={element.key}/>
                })}
            </div>
        )
    }


}

export default MainList;