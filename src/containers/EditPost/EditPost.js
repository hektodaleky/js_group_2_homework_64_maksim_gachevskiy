import React, {Component} from "react";
import axios from "axios";
import "./EditPost.css";

class EditPost extends Component {
    state = {
        post: {
            title: "",
            text: "",
            date: ""
        },
        key: ""
    };

    componentDidMount() {
        const query = new URLSearchParams(this.props.location.search);

        for (let param of query.entries()) {


            axios.get(`https://react-lesson-mgachevskiy.firebaseio.com/post/${param[0]}.json`).then(response => {
                this.setState({post: response.data, id: param[0]})
            })
        }


    };

    changeTitle = (event, name) => {
        let post = {...this.state.post};
        post[name] = event.target.value;
        this.setState({post});
    };
    savePost = () => {


        axios.put(`/post/${this.state.id}.json`, this.state.post
        ).then(() => {
            this.props.history.push({
                pathname: '/'
            });
        });
    };

    render() {
        return (
            <div className="full--post">
                <input className="edit--title" onChange={(event) => this.changeTitle(event, "title")}
                       value={this.state.post.title}/>
                <p className="item--date">{this.state.post.date}</p>
                <textarea className="edit--text" onChange={(event) => this.changeTitle(event, "text")}
                          value={this.state.post.text}></textarea>
                <button onClick={this.savePost}>Save</button>

            </div>

        )
    }
}
;
export default EditPost;