import React, {Component} from "react";
import axios from "axios";
class About extends Component {
    state = {
        about: {
            info: "",
            infoText: ""
        }

    };

    componentDidMount() {
        axios.get('https://react-lesson-mgachevskiy.firebaseio.com/about.json').then(response => {

            let about;
            for (let key in response.data) {

                about = response.data[key]


            }
            this.setState({about});

        })
    }

    render() {
        return (
            <div>
                <p>{this.state.about.info}</p>
                <p>{this.state.about.infoText}</p>

            </div>
        )
    }
}

export default About;