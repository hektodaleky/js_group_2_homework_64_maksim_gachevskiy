import React, {Component} from "react";
import axios from "axios";
class Contact extends Component {
    state = {
        contact: {
            cont: "",
            tel: ""
        }

    };

    componentDidMount() {
        axios.get('https://react-lesson-mgachevskiy.firebaseio.com/contact.json').then(response => {

            let contact;
            for (let key in response.data) {

                    contact = response.data[key]


            }
            this.setState({contact});

        })
    }

    render() {
        return (
            <div>
                <p>{this.state.contact.cont}</p>
                <p>{this.state.contact.tel}</p>

            </div>
        )
    }
}

export default Contact;