import React, {Component, Fragment} from "react";
import "./App.css";
import MainList from "./containers/MainList/MainList";
import {NavLink, Route, Switch} from "react-router-dom";
import AddPost from "./containers/AddPost/AddPost";
import FullPost from "./containers/FullPost/FullPost";
import EditPost from "./containers/EditPost/EditPost";
import About from "./containers/About/About";
import Contact from "./containers/Contact/Contact";

class App extends Component {
    AddPost;

    render() {
        return (
            <Fragment>
                <div className="menu"><h6>My Blog</h6>
                    <p><NavLink to="/home" exact>Home</NavLink></p>
                    <p><NavLink to="/add" exact>Add</NavLink></p>
                    <p><NavLink to="/about" exact>About</NavLink></p>
                    <p><NavLink to="/contact" exact>Contact</NavLink></p>
                </div>


                <Switch>
                    <Route path='/add' exact component={AddPost}/>
                    <Route path='/about' exact component={About}/>
                    <Route path='/contact' exact component={Contact}/>
                    <Route path="/post/edit/" component={EditPost}/>
                    <Route path='/post' component={FullPost}/>
                    <Route path='/home' component={MainList}/>

                    <Route path='/' exac component={MainList}/>


                </Switch>
            </Fragment>
        );
    }
}

export default App;
